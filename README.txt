DESCRIPTION:

Another module dealing with the new EU cookielaw.

This module provides a 'all or nothing' approach dealing with cookies. On a first-time visit, the visitor will get a splash screen asking to allow cookies. All other cookies for the site will get deleted. If the visitor accepts, a cookie will be set to remember the setting for one year. From there on, the site will function normally.

The problem with other drupal cookie modules like listed below (see related modules), is they don't really prevent cookies from being set until you accept, which is necessary for the Dutch version of the cookielaw, which is more strict than the European version. This is especially true for sites which use a lot of social media integration or media embeds (e.g. youtube). It would be very time-consuming to display such a site without all this content when a visitor chooses not to accept cookies. That's why many Dutch sites have implemented this somewhat crude 'cookiewall' solution asking the visitor to accept all cookies or they won't be able to see the site.

TECHNICAL NOTE:

I chose hook_page_alter to serve a completely seperate .html file instead of using the drupal theme/render system. This is done because want to be sure nothing is rendered or included (.js) that could set a cookie. 

RELATED MODULES:

- http://drupal.org/project/ro_cookie_opt_in
- http://drupal.org/project/eu-cookie-compliance
- http://drupal.org/project/cookiecontrol

INSTALLATION:

- Enable module
- There is a settings page under configuration -> system -> cookiewall. It allow to set some identifiers which are used to detect search engines and other non-humans (using USER AGENT). These won't get the splash screen.
- The settings page also provides a filename/path for the template used. You have to copy the cookiewall.html file and put it in the root of your theme. Also copy the cookiewall.jpg and cookiewall.css and place them in the /images and /styles folders of your theme. 
- You can also change the name and path of cookiewall.html in the settings screen. For example, you could provide template/cookiewall.html if you want don't want it in the root of your theme file.
- Change the css path in cookiewall.html appropriately.
- Make a screenshot of your frontpage and replace cookiewall.jpg with it. I recommend a gaussian blur and at least a resolution of 1600x1200.
- There are no permissions for this module.

TODO / ISSUES:

- Please use the project issue queue for any issues with this module.
- It might be cool to implement PHP's imagegrabscreen (http://php.net/manual/en/function.imagegrabscreen.php) to automatically create a screenshot.

CREDITS:

This module is sponsered by Merge (www.merge.nl) and developed by Albert Skibinski (@askibinski).